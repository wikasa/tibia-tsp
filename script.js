const start=[
    "Ab'Dendriel",
    "Ankrahmun",
    "Carlin",
    "Darashia",
    "Edron",
    "Liberty Bay",
    "Port Hope",
    "Roshamuul",
    "Oramond",
    "Svargrond",
    "Thais",
    "Venore",
    "Yalahar",
    "Krailos",
    "Issavi"
  ]

let distance=[
    [
      "Ab'Dendriel",
      "—",
      "—",
      "80",
      "—",
      "70",
      "—",
      "—",
      "—",
      "—",
      "—",
      "130",
      "90",
      "160",
      "—",
      "—"
    ],
    [
      "Ankrahmun",
      "—",
      "—",
      "—",
      "100",
      "160",
      "90",
      "80[3]",
      "—",
      "—",
      "—",
      "—",
      "150",
      "230",
      "—",
      "—"
    ],
    [
      "Carlin",
      "80",
      "—",
      "—",
      "—",
      "110",
      "—",
      "—",
      "—",
      "—",
      "110",
      "110",
      "130",
      "185",
      "—",
      "—"
    ],
    [
      "Darashia",
      "—",
      "100",
      "—",
      "—",
      "—",
      "200",
      "180",
      "—",
      "—",
      "—",
      "—",
      "60",
      "210",
      "110",
      "130"
    ],
    [
      "Edron",
      "70",
      "160",
      "110",
      "—",
      "—",
      "170",
      "150",
      "—",
      "—",
      "—",
      "160",
      "40",
      "—",
      "100",
      "—"
    ],
    [
      "Liberty Bay",
      "—",
      "90",
      "—",
      "200",
      "170",
      "—",
      "50",
      "—",
      "—",
      "—",
      "180",
      "180",
      "275",
      "—",
      "—"
    ],
    [
      "Port Hope",
      "—",
      "110[3]",
      "—",
      "180",
      "150",
      "50",
      "—",
      "—",
      "—",
      "—",
      "160",
      "160",
      "260",
      "—",
      "—"
    ],
    [
      "Roshamuul",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "210",
      "—",
      "—",
      "—",
      "—"
    ],
    [
      "Oramond",
      "—",
      "—",
      "—",
      "—",
      "110[1]",
      "—",
      "200[1]",
      "—",
      "—",
      "—",
      "150",
      "130[1]",
      "—",
      "60",
      "120[4]"
    ],
    [
      "Svargrond",
      "—",
      "—",
      "110",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "—",
      "180",
      "150",
      "—",
      "—",
      "—"
    ],
    [
      "Thais",
      "130",
      "—",
      "110",
      "—",
      "160",
      "180",
      "160",
      "210",
      "150",
      "180",
      "—",
      "170",
      "200",
      "—",
      "—"
    ],
    [
      "Venore",
      "90",
      "150",
      "130",
      "60",
      "40",
      "180",
      "160",
      "—",
      "—",
      "150",
      "170",
      "—",
      "185",
      "110",
      "130"
    ],
    [
      "Yalahar[2]",
      "160",
      "230",
      "185",
      "210",
      "—",
      "275",
      "260",
      "—",
      "—",
      "—",
      "200",
      "185",
      "—",
      "—",
      "—"
    ],
    [
      "Krailos",
      "—",
      "—",
      "—",
      "110",
      "100",
      "—",
      "—",
      "—",
      "60",
      "—",
      "—",
      "110",
      "—",
      "—",
      "70"
    ],
    [
      "Issavi",
      "—",
      "—",
      "—",
      "80",
      "—",
      "—",
      "—",
      "—",
      "100",
      "—",
      "—",
      "80",
      "—",
      "80",
      "—"
    ]
  ]

function assignElements(){
  let boat=distance.reduce((acc, item, index)=>{
    const objectWithPrices= item.reduce((acc,curr, index)=>{ 

      if(index!=0){
        acc[start[index-1]]=curr;
      } 

      return acc;   },{})
    acc[start[index]]=objectWithPrices;
    return acc;
  },{})
  return boat
}
console.log(assignElements())